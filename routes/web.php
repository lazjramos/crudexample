<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [
    'uses' => 'HomeController@index'
])->middleware('auth');

Auth::routes();

Route::get('/logout', function (){
    \Illuminate\Support\Facades\Auth::logout();
    return \Illuminate\Support\Facades\Redirect::to('/');
});

Route::group(['middleware' => 'isAdmin'],function (){
    Route::get('users','HomeController@fetchAllUsers');

    Route::post('user','HomeController@store');

    Route::delete('user/{id}','HomeController@destroy');

    Route::patch('user', 'HomeController@update');
});

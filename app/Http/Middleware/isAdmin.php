<?php

namespace App\Http\Middleware;

use Closure;

class isAdmin
{
    /**
     * Verifying is the user's role is administrator
     *
     * @param $request
     * @param Closure $next
     * @return \Illuminate\Http\JsonResponse|mixed
     */
    public function handle($request, Closure $next)
    {
        $user = $request->user();

        if($user){
            if($user->getRoleId() == 1){
                return $next($request);
            }
        }

        return response()->json([
            'message' => 'Access denied'
        ]);

    }
}

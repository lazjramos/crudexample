<?php

namespace App\Http\Controllers;

use App\Rol;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Redirect;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {

    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $privilege = Rol::find(Auth::user()->role_id)->rol;

      if ($privilege == 'admin'){
          return view('admin.dashboard');
      }
      elseif($privilege == 'users'){
          return view('users.dashboard');
      }
      else{
          Auth::logout();
          Redirect::to('login');
      }
    }

    /**
     * Get all users
     *
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function fetchAllUsers(){
        return DB::table('users')->select('id','name', 'email','role_id')->get();;
    }

    /**
     * Store a new user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request){
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'roleSelected' => 'required',
            'password' => 'required|same:password2'
        ]);

        $role = Rol::find($request->input('roleSelected'));

        $user = new User();
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->rol()->associate($role);
        $user->password = Hash::make($request->input('password'));
        $user->save();

        return response()->json([
            'message' => 'User created successfully',
            'user' => DB::table('users')->where('id','=',$user->id)->select(['id','name','email','role_id'])->get()->first()
        ]);
    }

    /**
     * Delete an User
     *
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy($id){

        User::destroy($id);

        return response()->json([
            'message' => 'User deleted Successfully'
        ]);
    }

    /**
     * Update a user
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request){

        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email_to_user:'.$request->input("id"),
            'roleSelected' => 'required',
            'password' => 'same:password2'
        ]);

        $rol = Rol::find($request->input('roleSelected'));

        $user = User::find($request->input('id'));
        $user->name = $request->input('name');
        $user->email = $request->input('email');
        $user->rol()->associate($rol);
        if ($request->input('password') != Null){
            $user->password = Hash::make($request->input('password'));
        }
        $user->save();

        return response()->json([
            'message' => 'User updated successfully'
        ]);
    }
}

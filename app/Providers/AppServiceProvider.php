<?php

namespace App\Providers;


use App\User;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Validator;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);

//        Custom Validation
        Validator::extend('email_to_user',function ($attribute, $value, $parameters, $validator){
            $id = $parameters[0];

            $user = User::where('email','=',$value)->first();

            if (count($user) > 0){
                if ($user->id == $id){
                    return true;
                }
                else{
                    return false;
                }
            }
            else{
                return true;
            }

        });

    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}

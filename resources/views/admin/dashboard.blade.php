<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel & Vue</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <link rel="stylesheet" href="/css/app.css">
        <script>
          window.Laravel = <?php echo json_encode([
              'csrfToken' => csrf_token(),
          ]); ?>
        </script>
        <style>
            #header {
                padding-bottom: 40px;
            }
        </style>
    </head>

    <body>
        <div class="container">
            <div class="row" id="header">
                <div class="col-sm-8 col-sm-offset-2">
                    <a href="logout" class="pull-right">Logout</a>
                    <span class="pull-left" id="greeting">Hola {{ \Illuminate\Support\Facades\Auth::user()->name }}</span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2">
                    <div id="app">
                        <ul class="nav nav-tabs">
                            <router-link tag="li" to="/" exact active-class="active">
                                <a href="#">Home</a>
                            </router-link>
                            <router-link tag="li" to="/about" active-class="active">
                                <a href="#">About</a>
                            </router-link>
                            <router-link tag="li" to="/crud" active-class="active">
                                <a href="#">Crud</a>
                            </router-link>
                        </ul>
                        <keep-alive>
                            <router-view></router-view>
                        </keep-alive>
                    </div>                    
                </div>
            </div>
        </div>

        <script src="/js/app.js" type="text/javascript"></script>
    </body>
</html>

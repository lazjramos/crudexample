import VueRouter from 'vue-router';

let routes = [
	{
		path: '/',
		component: require('./components/views/Home')
	},
	{
		path: '/about',
		component: require('./components/views/About')
	},
	{
		path: '/crud',
		component: require('./components/views/Crud')
	}
];

export default new VueRouter({
	routes
});
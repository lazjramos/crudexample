<?php

use Illuminate\Database\Seeder;
use App\User;
use App\Rol;
use Illuminate\Support\Facades\Hash;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Laz',
                'email' => 'laz@vue.dev',
                'password' => '1234',
                'role_type' => 'admin',
            ],
            [
                'name' => 'Alex',
                'email' => 'alex@vue.dev',
                'password' => '1234',
                'role_type' => 'admin',
            ],
            [
                'name' => 'Jhon Doe',
                'email' => 'jdoe@gmail.com',
                'password' => '1234',
                'role_type' => 'users',
            ]
        ];

        foreach ($users as $user)
        {
            $rol = Rol::where('rol','=',$user['role_type'])->first();

            $u = new User;
            $u->name = $user['name'];
            $u->email = $user['email'];
            $u->password = Hash::make($user['password']);
            $u->rol()->associate($rol);
            $u->save();
        }
    }
}

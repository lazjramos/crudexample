<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roles = [
            [
                'rol' => 'admin',
                'description' =>  'Usuarios con privilegios administrativos'
            ],
            [
                'rol' => 'users',
                'description' => 'Usuarios normales del sistema'
            ]
        ];

        foreach ($roles as $rol)
        {
            \App\Rol::create($rol);
        }
    }
}
